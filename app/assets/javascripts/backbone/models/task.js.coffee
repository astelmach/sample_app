class SimpleTodo.Models.Task extends Backbone.Model
class SimpleTodo.Collections.Tasks extends Backbone.Collection
  model: SimpleTodo.Models.Task
  url: '/tasks'
