class Task < ActiveRecord::Base
  attr_accessible :caption, :done
  belongs_to :user
  scope :done, ->{ where('1 = 1') }
end
